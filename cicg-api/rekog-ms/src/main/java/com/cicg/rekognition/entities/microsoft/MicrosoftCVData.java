package com.cicg.rekognition.entities.microsoft;

import com.cicg.rekognition.entities.microsoft.Category;
import java.util.List;

public class MicrosoftCVData {
    
    List<Category> categories;
    Adult adult;
    List<Tag> tags;
    Description description;
    String requestId;
    Metadata metadata;
    List<Face> faces;
    Color color;
    ImageType imageType;

    public MicrosoftCVData() {
    }

    public MicrosoftCVData(List<Category> categories, Adult adult, List<Tag> tags, Description description, String requestId, Metadata metadata, List<Face> faces, Color color, ImageType imageType) {
        this.categories = categories;
        this.adult = adult;
        this.tags = tags;
        this.description = description;
        this.requestId = requestId;
        this.metadata = metadata;
        this.faces = faces;
        this.color = color;
        this.imageType = imageType;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Adult getAdult() {
        return adult;
    }

    public void setAdult(Adult adult) {
        this.adult = adult;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<Face> getFaces() {
        return faces;
    }

    public void setFaces(List<Face> faces) {
        this.faces = faces;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public ImageType getImageType() {
        return imageType;
    }

    public void setImageType(ImageType imageType) {
        this.imageType = imageType;
    }
    
    
}
