package com.cicg.rekognition.controller;

import com.cicg.rekognition.entities.RekognitionDataInput;
import com.cicg.rekognition.services.RekognitionService;
import java.io.IOException;
import java.net.URISyntaxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RekogController {
    
    @Autowired
    RekognitionService rekognitionService;
        
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK) 
    public String rekognize(@RequestBody RekognitionDataInput dataInput) 
            throws URISyntaxException, IOException {
        return rekognitionService.rekognize(dataInput);
    }
}
