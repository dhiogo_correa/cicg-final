
package com.cicg.rekognition.entities.microsoft;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "clipArtType",
    "lineDrawingType"
})
public class ImageType {

    @JsonProperty("clipArtType")
    private Integer clipArtType;
    @JsonProperty("lineDrawingType")
    private Integer lineDrawingType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("clipArtType")
    public Integer getClipArtType() {
        return clipArtType;
    }

    @JsonProperty("clipArtType")
    public void setClipArtType(Integer clipArtType) {
        this.clipArtType = clipArtType;
    }

    @JsonProperty("lineDrawingType")
    public Integer getLineDrawingType() {
        return lineDrawingType;
    }

    @JsonProperty("lineDrawingType")
    public void setLineDrawingType(Integer lineDrawingType) {
        this.lineDrawingType = lineDrawingType;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
