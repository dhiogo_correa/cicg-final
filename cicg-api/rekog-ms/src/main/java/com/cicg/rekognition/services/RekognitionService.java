package com.cicg.rekognition.services;

import com.cicg.rekognition.entities.RekognitionDataInput;
import java.io.IOException;
import java.net.URISyntaxException;

public interface RekognitionService {
    
    public String rekognize(RekognitionDataInput dataInput)
            throws URISyntaxException, SecurityException, IOException;
    
}
