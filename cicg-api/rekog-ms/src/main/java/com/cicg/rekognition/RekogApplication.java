package com.cicg.rekognition;

import java.io.IOException;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@EnableDiscoveryClient
@RefreshScope
@SpringBootApplication
public class RekogApplication {
        
    public static void main(String[] args) throws IOException {
        new SpringApplicationBuilder(RekogApplication.class).run(args);
    }
}
