package com.cicg.rekognition.entities;

public class RekognitionDataInput {

    String keyword;
    String image;

    public RekognitionDataInput() {
    }

    public RekognitionDataInput(String keyword, String image) {
        this.keyword = keyword;
        this.image = image;
    }
    
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    
}
