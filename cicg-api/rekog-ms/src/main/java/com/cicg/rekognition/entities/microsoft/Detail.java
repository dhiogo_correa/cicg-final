
package com.cicg.rekognition.entities.microsoft;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "celebrities",
    "landmarks"
})
public class Detail {

    @JsonProperty("celebrities")
    private List<Celebrity> celebrities = null;
    @JsonProperty("landmarks")
    private List<Landmark> landmarks = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("celebrities")
    public List<Celebrity> getCelebrities() {
        return celebrities;
    }

    @JsonProperty("celebrities")
    public void setCelebrities(List<Celebrity> celebrities) {
        this.celebrities = celebrities;
    }

    @JsonProperty("landmarks")
    public List<Landmark> getLandmarks() {
        return landmarks;
    }

    @JsonProperty("landmarks")
    public void setLandmarks(List<Landmark> landmarks) {
        this.landmarks = landmarks;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
