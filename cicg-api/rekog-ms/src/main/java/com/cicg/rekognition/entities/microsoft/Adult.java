
package com.cicg.rekognition.entities.microsoft;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "isAdultContent",
    "isRacyContent",
    "adultScore",
    "racyScore"
})
public class Adult {

    @JsonProperty("isAdultContent")
    private Boolean isAdultContent;
    @JsonProperty("isRacyContent")
    private Boolean isRacyContent;
    @JsonProperty("adultScore")
    private Double adultScore;
    @JsonProperty("racyScore")
    private Double racyScore;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("isAdultContent")
    public Boolean getIsAdultContent() {
        return isAdultContent;
    }

    @JsonProperty("isAdultContent")
    public void setIsAdultContent(Boolean isAdultContent) {
        this.isAdultContent = isAdultContent;
    }

    @JsonProperty("isRacyContent")
    public Boolean getIsRacyContent() {
        return isRacyContent;
    }

    @JsonProperty("isRacyContent")
    public void setIsRacyContent(Boolean isRacyContent) {
        this.isRacyContent = isRacyContent;
    }

    @JsonProperty("adultScore")
    public Double getAdultScore() {
        return adultScore;
    }

    @JsonProperty("adultScore")
    public void setAdultScore(Double adultScore) {
        this.adultScore = adultScore;
    }

    @JsonProperty("racyScore")
    public Double getRacyScore() {
        return racyScore;
    }

    @JsonProperty("racyScore")
    public void setRacyScore(Double racyScore) {
        this.racyScore = racyScore;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
