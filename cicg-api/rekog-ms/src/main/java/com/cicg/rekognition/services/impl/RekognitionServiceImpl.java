package com.cicg.rekognition.services.impl;

import com.cicg.rekognition.entities.RekognitionDataInput;
import com.cicg.rekognition.entities.microsoft.Caption;
import com.cicg.rekognition.entities.microsoft.Face;
import com.cicg.rekognition.entities.microsoft.MicrosoftCVData;
import com.cicg.rekognition.entities.microsoft.Tag;
import com.cicg.rekognition.services.RekognitionService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rmtheis.yandtran.language.Language;
import com.rmtheis.yandtran.translate.Translate;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class RekognitionServiceImpl implements RekognitionService {

    //private static final String MICROSOFT_CV_URL = "https://eastus2.api.cognitive.microsoft.com/vision/v1.0/analyze?visualFeatures=Categories,Tags,Description,Faces";
    private static final String MICROSOFT_CV_URL = "https://eastus2.api.cognitive.microsoft.com/vision/v1.0/describe";

    private static final String MICROSOFT_CV_KEY = "81ba0d061aa24db4b9a0b01d066060bb";

    Gson gson;

    private static final Logger LOG = Logger.getLogger(RekognitionServiceImpl.class.getName());

    public RekognitionServiceImpl() {
        Translate.setKey("trnsl.1.1.20151117T170157Z.062c6dbb81142d42.4d7cdfdbba01c0ee3816e9f8510ec62d1570199e");
        gson = new GsonBuilder().serializeNulls().create();
    }

    @Override
    public String rekognize(RekognitionDataInput dataInput)
            throws URISyntaxException, SecurityException, IOException {
        String img = dataInput.getImage();
        String keyword = dataInput.getKeyword();

        try {
            String translatedText = Translate.execute(keyword, Language.PORTUGUESE, Language.ENGLISH);
            MicrosoftCVData cognitiveVision = analyze(img);

            System.out.println(translatedText);

            return checkAnalyze(translatedText, cognitiveVision);
        } catch (Exception ex) {
            Logger.getLogger(RekognitionServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }

    }

    private MicrosoftCVData analyze(String img) throws URISyntaxException, IOException {
//        byte[] decoded = Base64.decodeBase64(img);
        String prefix = "data:image/jpeg;base64,";
        if (!img.startsWith(prefix)) {
            throw new IllegalArgumentException();
        }
        img = img.substring(prefix.length());

        byte[] decoded = java.util.Base64.getDecoder().decode(img);

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(MICROSOFT_CV_URL)
                .post(RequestBody.create(com.squareup.okhttp.MediaType.parse("application/octet-stream"), decoded))
                .addHeader("ocp-apim-subscription-key", MICROSOFT_CV_KEY)
                .addHeader("content-type", "application/octet-stream")
                .build();

        Response response = client.newCall(request).execute();

        if (response.isSuccessful()) {
            String res = response.body().string();
            MicrosoftCVData data = gson.fromJson(res, MicrosoftCVData.class);
            return data;
        } else {
            return null;
        }
    }

    public static String read(InputStream input) throws IOException {
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
            return buffer.lines().collect(Collectors.joining("\n"));
        }
    }

    private String checkAnalyze(String keyword, MicrosoftCVData cognitiveVision) throws Exception {

        if (cognitiveVision != null) {
            List<String> tags = cognitiveVision.getDescription().getTags();

            if (tags == null) {
                return "";
            }

            List<String> match = tags.stream().filter(t -> t.contains(keyword)).collect(Collectors.toList());

            if (match.isEmpty()) {
                List<Caption> captions = cognitiveVision.getDescription().getCaptions()
                        .stream().filter(c -> c.getText().contains(keyword)).collect(Collectors.toList());

                if (captions.isEmpty()) {

                    String captionText = Translate.execute(cognitiveVision.
                            getDescription().getCaptions().get(0).getText(), Language.ENGLISH, Language.PORTUGUESE);
                    
                    return captionText;
                } else {
                    Caption c = captions.get(0);

                    String captionText = Translate.execute(c.getText(), Language.ENGLISH, Language.PORTUGUESE);

                    String response = captionText;
                    LOG.log(Level.INFO, response);
                    return response;
                }
            } else {
                String t = match.get(0);

                String captionText = Translate.execute(cognitiveVision.
                        getDescription().getCaptions().get(0).getText(), Language.ENGLISH, Language.PORTUGUESE);

                String response = captionText;
                LOG.log(Level.INFO, response);
                return response;
            }
        } else {
            throw new SecurityException("Erro ao conectar ao serviço cognitivo.");
        }
    }

}
