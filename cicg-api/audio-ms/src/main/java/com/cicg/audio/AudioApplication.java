package com.cicg.audio;

import java.io.IOException;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@EnableDiscoveryClient
@RefreshScope
@SpringBootApplication
public class AudioApplication {
        
    public static void main(String[] args) throws IOException {
        new SpringApplicationBuilder(AudioApplication.class).run(args);
    }
}
