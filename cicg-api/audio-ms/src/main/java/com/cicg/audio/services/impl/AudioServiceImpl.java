package com.cicg.audio.services.impl;

import com.cicg.audio.services.AudioService;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
public class AudioServiceImpl implements AudioService {

    private static final String MICROSOFT_CV_URL_TOKEN = "https://api.cognitive.microsoft.com/sts/v1.0/issueToken";

    private static final String MICROSOFT_CV_URL_SYNTHESIZE = "https://speech.platform.bing.com/synthesize";

    private static final String MICROSOFT_CV_URL_RECOGNIZE = "https://speech.platform.bing.com/recognize?scenarios=smd&appid=D4D52672-91D7-4C74-8AD8-42B1D98141A5&locale=pt-BR&device.os=WindowsOS&version=3.0&format=json&instanceid=b2c95ede-97eb-4c88-81e4-80f32d6aee54&requestid=b2c95ede-97eb-4c88-81e4-80f32d6aee54";

    private static final String MICROSOFT_CV_KEY = "0c7fcf92ee6b4b409388cbebd4822bf3";

    @Override
    public String audio_to_text(String audio) throws IOException {
        String token = getToken();

        byte[] decoded = java.util.Base64.getDecoder().decode(audio);

        Request request = new Request.Builder()
                .url(MICROSOFT_CV_URL_RECOGNIZE)
                .post(RequestBody.create(null, decoded))
                .addHeader("Authorization", "Bearer " + token)
                .addHeader("Content-Type", "audio/wav; codec=\"audio/pcm\"; samplerate=8000; result.profanitymarkup=0")
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = client.newCall(request).execute();

        if (response.isSuccessful()) {
            String res2 = response.body().string();
            return res2;
        } else {
            return "";
        }
    }

    @Override
    public byte[] text_to_audio(String text) throws IOException {
        text = text.replace("{", "")
                .replace("}", "")
                .replace("response", "")
                .replace(":", "");

        String token = getToken();

        if (token.isEmpty()) {
            throw new SecurityException("security failed");
        }

        OkHttpClient client = new OkHttpClient();

        String body = "<speak version=\"1.0\" xmlns=\"http://www.w3.org/2001/10/synthesis\" xmlns:mstts=\"http://www.w3.org/2001/mstts\" xml:lang=\"pt-BR\">"
                + "<voice xml:lang=\"pt-BR\" name=\"Microsoft Server Speech Text to Speech Voice (pt-BR, Daniel, Apollo)\">"
                + text
                + "</voice></speak>";

        Request request = new Request.Builder()
                .url(MICROSOFT_CV_URL_SYNTHESIZE)
                .post(RequestBody.create(null, body))
                .addHeader("Authorization", "Bearer " + token)
                .addHeader("X-Microsoft-OutputFormat", "audio-16khz-32kbitrate-mono-mp3")
                //                .addHeader("Content-Type", "application/ssml+xml")
                .addHeader("Content-Type", "audio/x-wav")
                .addHeader("X-Search-AppId", "07D3234E49CE426DAA29772419F436CA")
                .addHeader("X-Search-ClientID", "1ECFAE91408841A480F00935DC390960")
                .addHeader("User-Agent", "TTSAndroid")
                .addHeader("Accept", "*/*")
                .build();

        Response response = client.newCall(request).execute();

        if (response.isSuccessful()) {
            byte[] res2 = inputStreamToByteArray(response.body().byteStream());
            
            String id = UUID.randomUUID().toString();
            
            //String path = "C:\\Users\\Dhiogo\\workspace\\MyWebApp\\WebContent\\audio\\" + id + ".wav";
            //byteArrayToFile(res2, path);
            
            return res2;
            //return "http://10.97.21.249:8080/MyWebApp/WebContent/audio/" + id + ".mp3";
        } else {
            return null;
        }
    }

    private String getToken() throws IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(MICROSOFT_CV_URL_TOKEN)
                .post(RequestBody.create(null, new byte[0]))
                .addHeader("Ocp-Apim-Subscription-Key", MICROSOFT_CV_KEY)
                .build();

        Response response = client.newCall(request).execute();

        if (response.isSuccessful()) {
            String res = response.body().string();
            return res;
        } else {
            return "";
        }
    }

    public byte[] inputStreamToByteArray(InputStream inStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[8192];
        int bytesRead;
        while ((bytesRead = inStream.read(buffer)) > 0) {
            baos.write(buffer, 0, bytesRead);
        }
        return baos.toByteArray();
    }

    public void byteArrayToFile(byte[] byteArray, String outFilePath) throws FileNotFoundException, IOException {
        FileOutputStream fos = new FileOutputStream(outFilePath);
        fos.write(byteArray);
        fos.close();
    }
}
