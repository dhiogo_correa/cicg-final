package com.cicg.audio.controller;

import com.cicg.audio.services.AudioService;
import java.io.IOException;
import java.net.URISyntaxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AudioController {
    
     @Autowired
    AudioService rekognitionService;
        
    @RequestMapping(value = "/audio_to_text", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK) 
    public String audioToText(@RequestBody String audio) 
            throws URISyntaxException, IOException {
        return rekognitionService.audio_to_text(audio);
    }
    
    @RequestMapping(value = "/text_to_audio/{text}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK) 
    String textToAudio(@PathVariable String text) 
            throws URISyntaxException, IOException {
        byte[] arr = rekognitionService.text_to_audio(text);
        return java.util.Base64.getEncoder().encodeToString(arr);
    }
}
