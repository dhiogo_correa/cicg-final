package com.cicg.audio.services;

import java.io.IOException;
import java.io.InputStream;

public interface AudioService {
    
    String audio_to_text(String audio) throws IOException;
    byte[] text_to_audio(String text) throws IOException;
}
