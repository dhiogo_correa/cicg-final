package com.itera.alice;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableAutoConfiguration
@EnableEurekaServer
@ComponentScan("test.builder")
public class EurekaApplication {


    public static void main(String[] args) throws Exception {
        //SpringApplication.run(EurekaApplication.class, args);
        new SpringApplicationBuilder(EurekaApplication.class).properties(
				"spring.config.name:eureka", "logging.level.com.netflix.discovery:OFF")
				.run(args);
    }

}
